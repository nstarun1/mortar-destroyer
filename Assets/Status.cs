﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Status : MonoBehaviour {
	public Text statusMessage;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		float hitpoints = transform.root.GetComponent<TankHealth> ().hitPoints;
		float bearing = transform.root.rotation.eulerAngles.y;
		statusMessage.text = "Health: \n" + hitpoints.ToString ("F0") + "\nBearing\n" + bearing.ToString("F0");



	}
}
