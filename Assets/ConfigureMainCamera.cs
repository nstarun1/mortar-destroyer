﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigureMainCamera : MonoBehaviour {
	public Camera mainCam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public void ConfigureCamera(Rect viewPortCoords) {

		mainCam.rect = viewPortCoords;


	}
}
