﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Test : MonoBehaviour {
	public Dropdown playerDropDown;
	// Use this for initialization

	void Start () {
		playerDropDown.onValueChanged.AddListener (delegate {
			OnMyValueChanged (playerDropDown);
		});
			}
	
	// Update is called once per frame
	public void OnMyValueChanged (Dropdown dd) {
		
		GameObject.Find ("DoNotDestroy").GetComponent<Utilities> ().Players   = dd.value+1;

	}
}

