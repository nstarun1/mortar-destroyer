﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour {
	public float hitPoints = 100;
	public Slider healthBar;
	public float startHitpoints;
	float healthPercent;
	public GameObject fill;
	public AudioSource clank;

	public List<Color> healthColors;
	public List<float> thresHolds;

	ColorBlock sliderColors;


	// Use this for initialization
	void Start () {
		startHitpoints = hitPoints;
		sliderColors = healthBar.colors;
	}

	Color healthBarColor (){
		
		for (int i = 0; i < thresHolds.Count - 1; i++) {

			if (healthPercent > thresHolds [i + 1]) {
				return healthColors [i];
			}

		}

		return Color.red;

	}

	
	// Update is called once per frame
	void Update () {
		

		healthPercent = hitPoints / startHitpoints;

		//health.GetComponentInChildren<Text>().text= hitPoints.ToString ("F2")+"%";

		//print (fill.GetComponent<Image> ().color);

		fill.GetComponent<Image>().color= healthBarColor ();



		healthBar.value = healthPercent;

		if (transform.position.y<-100){Destroy (gameObject, 1f);}

		}






	//bool inrender =false;
	void CollateralDamage(float damage){



		//StartCoroutine (FlashTank ());
		clank.Play();

		hitPoints -= damage;
		if (hitPoints < 0f) {
			GetComponent<BlowUpTank> ().Explode ();
			GetComponent<TankData> ().alive = false;
			transform.Find("GUICanvas").gameObject.SetActive(false);
		}


	}

	public bool IsAlive()
	{
		return hitPoints > 0f;
	}


	IEnumerator FlashTank(){

		GlowTank (Color.red);
		yield return new WaitForSeconds (3);
		GlowTank(Color.black);
		


	}





		void GlowTank(Color color){

	
	
		MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer> ();

		foreach (MeshRenderer r in renderers) {
			

			foreach (Material m in r.materials) {

						
				m.EnableKeyword ("_EMISSION");

				m.SetColor ("_EmissionColor", color);
			}
			
					
			}
	
		}

	


//	void OnCollisionEnter (Collision collision)
//	{
//		//print (hitPoints);
//
//
//		if (collision.gameObject.tag == "Shell") {
//				
//			hitPoints -= collision.relativeVelocity.magnitude;
//			if (hitPoints < 0f) {
//				GetComponent<BlowUpTank> ().Explode ();
//		
//			}
//		}
//	}
}

