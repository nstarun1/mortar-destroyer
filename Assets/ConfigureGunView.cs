﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ConfigureGunView : MonoBehaviour {
	public Camera gunCam;
	public RawImage gunContents;
	// Use this for initialization
	void Start () {
		//Configure ();
	}
	
	// Update is called once per frame
	public void Configure () {
		//gunCam.aspect = 1f;
		Vector2 size = gunContents.GetComponent<RectTransform>().rect.size;
		//		

		gunCam.aspect = size.x / size.y;
		RenderTexture contents = new RenderTexture((int) size.x, (int) size.y, 32, RenderTextureFormat.ARGB32);
		gunCam.targetTexture = contents;
		gunContents.texture =contents;
		//
		
	}
}
