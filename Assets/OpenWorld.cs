﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class OpenWorld : MonoBehaviour {
	public Button btnOpenWorld;
	public string sceneName;

	void Awake(){
	
		//DontDestroyOnLoad (this.gameObject);

	}

	// Use this for initialization
	void Start () {

		btnOpenWorld = GetComponent<Button> ();


		btnOpenWorld.onClick.AddListener (OpenScene);


	}
	
	// Update is called once per frame
	public void OpenScene () {
		SceneManager.LoadScene (sceneName);
	}
}
