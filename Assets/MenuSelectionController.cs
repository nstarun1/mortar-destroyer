﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class MenuSelectionController : MonoBehaviour
{
	public MenuSelection[] menuSelections;
	public int startIndex;

	private string _leftRightInputAxis = "XBox360_Horizontal_1";
	private string _upDownInputAxis = "XBox360_Vertical_1";
	
	private KeyCode _selectButton = KeyCode.Joystick1Button16;
	private int _currentIndex;
	private bool _allowMove;

	void Start()
	{
		_currentIndex = startIndex;
		UpdateMenu();
	}
	
	void Update()
	{		
		float leftRight = Input.GetAxis(_leftRightInputAxis);
		float upDown = Input.GetAxis(_upDownInputAxis);

		if (_allowMove && leftRight == -1)
		{
			MoveLeft();

			_allowMove = false;
		}
		else if (_allowMove && leftRight == 1)
		{
			MoveRight();

			_allowMove = false;
		}
		else if (_allowMove && upDown == 1)
		{
			MoveUp();

			_allowMove = false;
		}
		else if (_allowMove && upDown == -1)
		{
			MoveDown();

			_allowMove = false;
		}
		else if (leftRight == 0 && upDown == 0)
		{
			_allowMove = true;
		}

		if (Input.GetKeyDown(_selectButton))
		{
			Selected();
		}
	}
	
	private void MoveLeft()
	{
		_currentIndex = Mathf.Clamp(--_currentIndex, 0, menuSelections.Length - 1);
		
		UpdateMenu();
	}

	private void MoveRight()
	{
		_currentIndex = Mathf.Clamp(++_currentIndex, 0, menuSelections.Length - 1);
		
		UpdateMenu();
	}

	private void MoveUp()
	{
		if (!menuSelections[_currentIndex].MoveUp())
		{
			_currentIndex = 0;
			
			UpdateMenu();
		}
	}

	private void MoveDown()
	{
		menuSelections[_currentIndex].MoveDown();
	}

	private void Selected()
	{
		menuSelections[_currentIndex].Selected();
	}

	private void UpdateMenu()
	{
		foreach (MenuSelection menuSelection in menuSelections)
		{
			menuSelection.TurnOff();
		}

		menuSelections[_currentIndex].TurnOn();
	}
}
