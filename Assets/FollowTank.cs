﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTank : MonoBehaviour {
	public Transform tankTransform;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = tankTransform.position + new Vector3 (0, 50, 0);

	}
}
