﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrack : MonoBehaviour {
	public AudioSource source;
	public List <AudioClip> clips;
	// Use this for initialization
	void Start () {


		source.clip=GetClip();
		source.Play ();
	}
	
	// Update is called once per frame
	void Update () {

		if (!source.isPlaying) {
			source.clip = GetClip ();
			source.PlayDelayed (2f);

		}



	}

	AudioClip GetClip()
	{ int choice = Random.Range (0, clips.Count - 1);

		return clips[choice];
	}

}
