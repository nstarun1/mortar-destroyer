using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;
public class Initiate : MonoBehaviour {
    public bool spawnComplete=false;
	public SpawnTanks spawnTank;
	public DeathMatchGameMode deathMatchGameMode;


    // Use this for initialization
    void Start () {
		
	    spawnTank.SpawnAll ();

	    if (deathMatchGameMode)
	    {
		    deathMatchGameMode.SetTanks(spawnTank._tanks);
	    }
	    
        spawnComplete = true;

    }
	
    // Update is called once per frame
    void Update () {
		
    }
}
