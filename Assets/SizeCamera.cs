﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeCamera : MonoBehaviour {
	
	// Use this for initialization

	public List <GameObject> enemies =new List<GameObject>();
	public List <Vector3> vectorEnemies =new List<Vector3>();
	public List <float> distancesToEnemies=new List<float>();

	Transform enemyTransform;
	Vector3 lineToEnemy;
	float distanceToEnemy;
	float maxDist=0;
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {
		enemies.Clear ();
		vectorEnemies.Clear ();
		distancesToEnemies.Clear ();



		foreach (GameObject tank in GameObject.FindGameObjectsWithTag ("Tank")) {


			if (transform.root.gameObject.name != tank.name) {
				enemies.Add (tank);
				Vector3 lineToEnemy = tank.transform.position - transform.position;
				vectorEnemies.Add (lineToEnemy);
				distancesToEnemies.Add (lineToEnemy.magnitude);
				if (lineToEnemy.magnitude > maxDist) {
					maxDist = lineToEnemy.magnitude;
				}
			}


		}


		GetComponent<Camera> ().orthographicSize = maxDist * 1f;
		//transform.LookAt ((farthestEnemy ().transform.position-transform.position)/2f);

		//travelCamera.fieldOfView = Mathf.Atan (100 / 2 / dista)*Mathf.Rad2Deg;
	}

	public float DistanceBetween(GameObject one, GameObject two){
	
		return (two.transform.position - one.transform.position).magnitude;
	
	}




	public GameObject farthestEnemy(){
		float farthestDist = 0;
		int farthestIndex=0;
		for (int i = 0; i < distancesToEnemies.Count; i++) {

			if (distancesToEnemies[i]>farthestDist) {
				farthestDist=distancesToEnemies[i];
				farthestIndex = i;
			}

		}

		return enemies [farthestIndex];
	}









	public GameObject closestEnemy(){
		float closestDist = 99999999;
		int closestIndex=0;
		for (int i = 0; i < distancesToEnemies.Count; i++) {
		
			if (distancesToEnemies[i]<closestDist) {
				closestDist=distancesToEnemies[i];
				closestIndex = i;
			}
		
		}
	
		return enemies [closestIndex];
	}


	}


