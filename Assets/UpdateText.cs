﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class UpdateText : MonoBehaviour {
	public Transform gunTip;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{


		GetComponent<Slider> ().value = gunTip.rotation.x * 180f;

		GetComponent<Text> ().text = GetComponent<Slider> ().value.ToString ("F0") ;
	}
}
