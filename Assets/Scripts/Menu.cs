﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System;
using Complete;

public class Menu : MonoBehaviour {

	public Text seed;
    public Toggle useFalloff;
    public GameObject playersContainer;
    
   
    private MapGenerator mapGenerator;
    private MapDisplay mapDisplay;
    private int testIndex;
    
    public DeathMatchGameMode deathMatchGameMode;

    public void Start () {


        mapGenerator = FindObjectOfType<MapGenerator> ();
        mapDisplay = FindObjectOfType<MapDisplay> ();

        GenerateMap ();


		GameObject.Find ("SpawnTank").GetComponent<SpawnTanks> ().SpawnAll ();
        deathMatchGameMode.SetTanks(GameObject.Find ("SpawnTank").GetComponent<SpawnTanks> ()._tanks);




        //PlacePlayers ();
    }

    private void GenerateMap ()
    {
        var rnd = new System.Random();
        mapGenerator.seed = rnd.Next(1, 1000);
		mapGenerator.useFalloff = useFalloff.isOn;
		mapGenerator.drawMode = MapGenerator.DrawMode.Mesh;
		mapGenerator.DrawMap ();
		gameObject.SetActive (false);

	}

    
	public Vector3 GetRandomPosition () {
        

			Mesh mesh =mapDisplay.meshFilter.sharedMesh;

		Vector3 position = GetScaledPlayerPosition (mesh.vertices);
//        GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
        RaycastHit hit;
        if (Physics.Raycast (new Vector3 (position.x, mapDisplay.meshRenderer.bounds.max.y + 5f, position.z), -Vector3.up, out hit)) {
            //            print (mapDisplay.renderer.bounds.max.y);
            Debug.DrawRay (new Vector3 (position.x, mapDisplay.meshRenderer.bounds.max.y + 5f, position.z), -Vector3.up, Color.red, 100000, false);
            // the raycast hit, the point on the terrain is hit.point, spawn a tree there
            position = hit.point + new Vector3 (0f, 1f, 0f);
        } else {
            // the raycast didn't hit, maybe there's a hole in the terrain?
            print ("else");
        }
//        cube.transform.position = position;
//        cube.AddComponent (typeof(Rigidbody));
//        cube.transform.parent = playersContainer.transform;
//        GameObject player = Instantiate(playerTank);
//        player.transform.position = position;
//        transform.transform.parent = playersContainer.transform;

        return position;
    
	}





    private Vector3 GetScaledPlayerPosition (Vector3[] vertices) {
        Vector3 startPosition = GetValidStartPosition (vertices);
        float xScale = mapDisplay.meshFilter.transform.localScale.x;
        float zScale = mapDisplay.meshFilter.transform.localScale.z;
        return new Vector3 (startPosition.x * xScale, startPosition.y, startPosition.z * zScale);
    }

    public Vector3 GetValidStartPosition (Vector3[] vertices) {
        Vector3 startPosition = new Vector3 (0f, 0f, 0f);
        while (startPosition.x == 0f) {
            float floatIndex = UnityEngine.Random.Range (0f, (float)vertices.Length);
            testIndex = Mathf.RoundToInt (floatIndex);
            TerrainType terrain = Array.Find (mapGenerator.regions, GetRegionByColor);
            if (terrain.canInitPlayer) {
                startPosition = vertices [testIndex];
            }
        }
        return startPosition;
    }

    private bool GetRegionByColor (TerrainType terrain) {
        return mapGenerator.colorMap [testIndex] == terrain.color;
    }

}
