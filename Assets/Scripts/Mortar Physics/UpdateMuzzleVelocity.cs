﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateMuzzleVelocity : MonoBehaviour {
	public Slider slider;
	public float muzzleText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject.Find("MuzzleVelocityText").GetComponent<Text>().text = (slider.value).ToString();

	}
}
