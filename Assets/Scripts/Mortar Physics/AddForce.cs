﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour {


	ParticleSystem.Particle[] points;
	// Use this for initialization
	void Start () {
		//GetComponent<Rigidbody> ().AddForce (0, 100f, 0);

		points = new ParticleSystem.Particle[10];


		for (int i = 0; i < points.GetLength (0); i++) {
			points [i].position = new Vector3 (i, i, i);
			points [i].startColor = new Color (i, 0, 0);
			points [i].startSize = 0.2f;
		
		
		}

		GetComponent<ParticleSystem> ().SetParticles (points, points.Length);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
