﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//public class PlayerInput : MonoBehaviour
//{
//    [SerializeField] private Transform _camera;
//    [SerializeField] private PlayerController _playerController;
//    [SerializeField] private string horizontalMoveInput;
//    [SerializeField] private string verticalMoveInput;
//    [SerializeField] private string horizontalLookInput;
//    [SerializeField] private string verticalLookInput;
//
//    private Vector3 move;
//    private Vector3 look;
//    
//    private float horizontalMove = 0f;
//    private float verticalMove = 0f;
//    private float horizontalLook = 0f;
//    private float verticalLook = 0f;
//    
//    private void Update()
//    {
//        horizontalMove = Input.GetAxis(horizontalMoveInput);
//        verticalMove = Input.GetAxis(verticalMoveInput);
//        
//        horizontalLook = Input.GetAxis(horizontalLookInput);
//        verticalLook = Input.GetAxis(verticalLookInput);
//
//        move = new Vector3(horizontalMove, 0, verticalMove).normalized;
//        look = new Vector3(horizontalLook, verticalLook, 0).normalized;
//                
//        _playerController.Move(move);
//        _playerController.Look(look);
//    }
//}
