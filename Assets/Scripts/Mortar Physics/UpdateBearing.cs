﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UpdateBearing : MonoBehaviour {
	public Slider slider;
	public Transform gunTip;
	public Text bearingText;

	void Update(){
		
		slider.value = gunTip.rotation.eulerAngles.y;
		bearingText.text = slider.value.ToString ("F0");
	
	}



	}
