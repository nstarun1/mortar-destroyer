﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UpdateGroundAngle : MonoBehaviour {

	public Slider slider;
	Transform gun;
	float angle;
	public void Start(){

		gun=GameObject.Find("Gun").GetComponent<Transform>();


		slider.onValueChanged.AddListener (delegate {
			ValueChangeCheck ();

		

		});

	}



	void ValueChangeCheck(){

		angle = slider.value;


		Quaternion rotation = Quaternion.Euler (-angle, gun.rotation.eulerAngles.y, gun.rotation.eulerAngles.z);

		gun.rotation = rotation;	

		GameObject.Find ("GroundAngleText").GetComponent<Text> ().text = string.Format("{0:0}",GameObject.Find ("SliderGroundAngle").GetComponent<Slider> ().value);
		//print(GameObject.Find("Gun").GetComponent<Transform>().rotation.eulerAngles);



	}

}
