﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireWeapon : MonoBehaviour {
	public Button fireButton;
	public GameObject cannonBall;
	public Transform gunTransform;
	public Slider muzzleSpeed;
	public Slider groundAngle;
	public Slider bearing;
	//public ParticleSystem bulletTrace;

	public int tsteps;
	public int resolution;

	Vector3 origin;
	Vector3 muzzleVelocity;
	private ParticleSystem.Particle[] points;
	float gravity = -9.8f;
	// Use this for initialization
	public Transform gunTip;
	public GameObject cube;
	public GameObject cameraMain;
	public GameObject cameraTravel;
	public GameObject GUI;

	GameObject travelCam;
	GameObject ball;

	void Start () {


		fireButton.onClick.AddListener (delegate {
			onFireClicked ();
		});

	}
	
	// Update is called once per frame
	void onFireClicked () {




		ball = GameObject.Instantiate (cannonBall);
		ball.GetComponent<Transform> ().position = GameObject.Find ("GunTip").GetComponent<Transform>().position;



		float bearingRad = Mathf.Deg2Rad*bearing.GetComponent<Slider>().value;

		float inclinationAngleRad = Mathf.Deg2Rad*groundAngle.GetComponent<Slider>().value;


		Vector3 weaponDirection = new Vector3 (Mathf.Cos (inclinationAngleRad)*Mathf.Sin(bearingRad), Mathf.Sin (inclinationAngleRad), Mathf.Cos(inclinationAngleRad)*Mathf.Cos(bearingRad));

		muzzleVelocity = weaponDirection * muzzleSpeed.GetComponent<Slider>().value;
		print (weaponDirection + " Direction");
		print (muzzleSpeed.GetComponent<Slider>().value.ToString()+ " speed" );
		print (muzzleVelocity + " Muzzle Velocity");



		ball.GetComponent<Rigidbody>().velocity = muzzleVelocity;

		origin =transform.TransformPoint(gunTip.localPosition);


		DrawTrace ();

		travelCam = Instantiate (cameraTravel);
		cameraMain.SetActive (false);
		GUI.SetActive (false);

	




	}

	void Update(){
		if (cameraTravel.activeSelf && ball) {
			
			travelCam.GetComponent<Transform> ().position = ball.GetComponent<Transform>().position + new Vector3 (0, 0, -20);
			travelCam.GetComponent<Transform> ().rotation = ball.GetComponent<Transform> ().rotation;

		
		}


	}



	void DrawTrace(){


		CreatePoints ();


		GetComponent<ParticleSystem> ().SetParticles (points,points.Length);		
		
	}
			


			void CreatePoints(){

		int point_count = 30;
		points = new ParticleSystem.Particle[point_count];
		float total_time = 30f;

		float increment = .2f;
		float current_time = 0;

		for (int i = 0; i < point_count; i++) {
					//float x = i * increment;

			current_time += increment;

			points [i].position =  positionAtT(current_time);

		//	Instantiate (cube);
		//	cube.GetComponent<Transform> ().position = points [i].position;

			print (points [i].position + " at point");
			//print (i);
			points [i].startColor = new Color (i, 0, 0);
			points [i].size = .5f;
			//print (points [i].position);


				}
			

}	


	Vector3 positionAtT(float t){

		/*
		float x = origin.x*0+ muzzleVelocity.x * t;
		float y = origin.y*0 + muzzleVelocity.y*t + .5f * 10*gravity * t*t;
		float z = origin.z*0 + muzzleVelocity.z * t;
		*/
		return  origin+muzzleVelocity * t + new Vector3 (0, .5f  * gravity * t * t, 0);


	}

}







