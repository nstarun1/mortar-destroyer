﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float lookSpeed;

    private Vector3 _currentCameraPosition;
    
    private void Awake()
    {
        SetCameraPosition();

        _rigidbody.velocity = new Vector3(1f, 0f, 1f);
    }

    private void Update()
    {
        Vector3 newPosition = _rigidbody.transform.position + _currentCameraPosition;

        if (newPosition != _camera.transform.position)
        {
            _camera.transform.position = _rigidbody.transform.position + _currentCameraPosition;
        }
        
        _camera.transform.LookAt(_rigidbody.transform);
    }
    
    public void Move(Vector3 moveDirection)
    {
        if (moveDirection != Vector3.zero)
        {
            moveDirection = (_camera.transform.right * moveDirection.x + _camera.transform.forward * moveDirection.z).normalized;
            moveDirection = new Vector3(moveDirection.x, 0, moveDirection.z);
            _rigidbody.AddForce(moveDirection * moveSpeed);
        }
    }

    public void Look(Vector3 lookDirection)
    {
        if (lookDirection != Vector3.zero)
        {
            _camera.transform.Translate(lookDirection * lookSpeed);
            _camera.transform.LookAt(_rigidbody.transform);
                
            SetCameraPosition();
        }
    }

    private void SetCameraPosition()
    {
        _currentCameraPosition = _camera.transform.position - _rigidbody.transform.position;
    }
}
