﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapPointOfInterest : MonoBehaviour
{
    [SerializeField] [Range(0f, 359f)] private float _heading;

    private RectTransform _rectTransform;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
    
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void SetHeading(float heading)
    {
        _heading = heading;
        
        Quaternion rotation = Quaternion.Euler(new Vector3(0f, 0f, -_heading));

        _rectTransform.rotation = rotation;
    }
    
    private void OnValidate()
    {
        Initialize();
        SetHeading(_heading);
    }
}
