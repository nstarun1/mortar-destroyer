﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MiniMapController : MonoBehaviour
{
	[SerializeField] private GameObject _pointOfInterestPrefab;
	[SerializeField] private GameObject _CompassPrefab;
	[SerializeField] private Camera _camera;
	[SerializeField] private Transform _target;
	[SerializeField] private RawImage _cameraView;
	[SerializeField] private GameObject _pointsOfInterestContainer;

	private RenderTexture _renderTexture;
	private Vector3 _cameraOffset;

	private Dictionary<MiniMapPointOfInterest, GameObject> _pointsOfInterest;

	private void Start()
	{
		ConfigureRenderTexture();

		_cameraOffset = _camera.transform.position - _target.position;

		FindPointsOfInterest();
	}

	private void FixedUpdate()
	{
		_camera.transform.position = _target.position + _cameraOffset;

		UpdatePointsOfInterest();
	}

	public void FindPointsOfInterest()
	{
		_pointsOfInterest = new Dictionary<MiniMapPointOfInterest, GameObject> ();

		for (int i = 0; i < _pointsOfInterestContainer.transform.childCount; i++) {

			//			if (_pointsOfInterestContainer.transform.GetChild (i).gameObject.tag != "Compass") {


			Destroy (_pointsOfInterestContainer.transform.GetChild (i).gameObject);
			//			}

		}

		GameObject[] pointOfInterests = GameObject.FindGameObjectsWithTag ("MiniMapPointsOfInterest");
		Color c;


		//print (this.transform.root.gameObject.name);

		for (int i = 0; i < pointOfInterests.Length; i++) {
		
			//print (pointOfInterests [i].transform.root.gameObject.name);
			//	print (i + " " + pointOfInterests [i].transform.parent.gameObject.name);

			if (pointOfInterests [i].transform.parent.gameObject != _target.gameObject) {
				
						
//				foreach (MeshRenderer m in pointOfInterests [i].transform.root.GetComponentsInChildren<MeshRenderer> ()) {
//
//					if (m.gameObject.name == "MiniMapMarker2") {
//					
//						print (m.GetComponent<MeshRenderer> ().materials [0].GetVector("_CamoBlackTintColor"));
//					};
//
//				}

					//Color c = pointOfInterests [i].transform.root.GetComponentInChildren<MeshRenderer> ().material.GetColor ("_CamoBlackTint");
				c=Color.red;

				if (pointOfInterests[i].transform.parent.gameObject.name == "PirateFlag")
				{
					c = Color.green;
				} else if (pointOfInterests[i].transform.parent.gameObject.name == "Target")
				{
					c = Color.blue;
				}

					//Color c = pointOfInterests [i].transform.root.GetComponentInChildren<MeshRenderer> ().material.color;
					CreatePointOfInterest (pointOfInterests [i].transform.parent.gameObject, c);
				}
			}

			UpdatePointsOfInterest ();
		}
							

	private void CreatePointOfInterest(GameObject gameObject, Color c)
	{	
		MiniMapPointOfInterest pointOfInterest;

		if (gameObject.name == "TrueNorthContainer") {
			return;
	
			pointOfInterest = Instantiate (_CompassPrefab, _pointsOfInterestContainer.transform, false).GetComponent<MiniMapPointOfInterest> ();
		} else {
			pointOfInterest = Instantiate (_pointOfInterestPrefab, _pointsOfInterestContainer.transform, false).GetComponent<MiniMapPointOfInterest> ();
		}




	
		pointOfInterest.GetComponent<RawImage> ().color = c;

		_pointsOfInterest[pointOfInterest] = gameObject;
	}

	private void UpdatePointsOfInterest()
	{
		foreach(KeyValuePair<MiniMapPointOfInterest, GameObject> miniMapEntry in _pointsOfInterest)
		{

			try{
				if (miniMapEntry.Key.GetComponent<RawImage>().color == Color.green)
				{
					miniMapEntry.Key.gameObject.transform.SetAsLastSibling();
				}
				
				Vector3 targetPosition = _target.transform.position;
				Vector3 pointOfInterestPosition = miniMapEntry.Value.transform.position;
				Vector3 targetPositionXZ =new Vector3(_target.transform.position.x,0,_target.transform.position.z);
				Vector3 pointOfInterestPositionXZ=new Vector3(pointOfInterestPosition.x,0, pointOfInterestPosition.z);






				Vector2 targetPosition2D =new Vector2(_target.transform.position.x,_target.transform.position.z);
				Vector2 pointOfInterestPosition2D=new Vector2(miniMapEntry.Value.transform.position.x,miniMapEntry.Value.transform.position.z);

				Vector3 targetToPOIXZ = pointOfInterestPositionXZ - targetPositionXZ;

				Vector3 targetForward= _target.transform.forward;
				Vector3 targetToPOINormalized = targetToPOIXZ.normalized;
				Vector3 targetForwardsNormalized=targetForward.normalized;
				float bearing;

				//cross product is negative when the point of interest is the the left of target
				Vector3 crossProduct =Vector3.Cross(targetForwardsNormalized,targetToPOINormalized);
				//angle always between 0-180 deg
				float angle = Vector3.Angle(targetForwardsNormalized, targetToPOINormalized);

				//if poi is left of target facing vector, make bearing referenced left of 360 degrees, otherwise use regular angle. 
				if (crossProduct.y<0f){bearing=360f-angle;} 
				else {bearing=angle;}

			

				//print(this.transform.root.gameObject.name+"    " +  crossProduct);
				Vector3 gameObjectPositionInViewport = _camera.WorldToViewportPoint(miniMapEntry.Value.transform.position);

				if (gameObjectPositionInViewport.x >= 0f && gameObjectPositionInViewport.x <= 1.0f && gameObjectPositionInViewport.y >= 0f && gameObjectPositionInViewport.y <= 1.0f)
				{
					miniMapEntry.Key.Hide();
				}
				else
				{
					miniMapEntry.Key.Show();
				}



				miniMapEntry.Key.SetHeading(bearing);
			}catch {
			}


		}
	}

	private void ConfigureRenderTexture()
	{
		_camera.aspect = 1f;

		Vector2 size = _cameraView.GetComponent<RectTransform>().rect.size;

		_renderTexture = new RenderTexture((int) size.x, (int) size.y, 32, RenderTextureFormat.ARGB32);
		_camera.targetTexture = _renderTexture;
		_cameraView.texture = _renderTexture;
	}

	private void OnValidate()
	{
//		ConfigureRenderTexture();
	}
}
