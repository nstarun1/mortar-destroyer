﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Complete {
public class MiniMapMarker : MonoBehaviour
{
    [SerializeField] private MeshRenderer _meshRenderer;
    [SerializeField] private Material _material;
    [SerializeField] private Texture2D _texture;
	public  Color _color;
    [SerializeField] private Transform _target;

    public bool configureSize = true;

    public void Awake()
    {
        if (configureSize)
        {
            ConfigureSize();
        }
        else
        {
            transform.localPosition = new Vector3(0f, _target.transform.position.y, 0f);
        }
        ConfigureMaterial();
    }

    public void ConfigureSize()
    {
        transform.localScale = _target.transform.localScale * 0.5f;
        transform.localPosition = new Vector3(0f, _target.transform.position.y, 0f);
    }
    
    private void ConfigureMaterial()
    {
        _meshRenderer.materials = new Material[1];
        _meshRenderer.materials[0] = new Material(_material);
        _meshRenderer.materials[0].mainTexture = _texture;
		_meshRenderer.materials[0].color = _color ;
		_meshRenderer.materials[0].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        _meshRenderer.materials[0].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        _meshRenderer.materials[0].SetInt("_ZWrite", 0);
        _meshRenderer.materials[0].DisableKeyword("_ALPHATEST_ON");
        _meshRenderer.materials[0].DisableKeyword("_ALPHABLEND_ON");
        _meshRenderer.materials[0].EnableKeyword("_ALPHAPREMULTIPLY_ON");
        _meshRenderer.materials[0].EnableKeyword("_SPECULARHIGHLIGHTS_OFF");
        _meshRenderer.materials[0].SetFloat("_SpecularHighlights",0f);
        _meshRenderer.materials[0].renderQueue = 3000;        
    }
}
}