﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator {

    public static MeshData GenerateMesh (float[,] heightMap, float heightMultiplier, AnimationCurve _meshHeightCurve, 
                                         int levelOfDetail) {
        // Since this will be run in multiple threads, create an copy of the AnimationCurve to avoid race condition
        AnimationCurve meshHeightCurve = new AnimationCurve (_meshHeightCurve.keys);

        int width = heightMap.GetLength (0);
        int height = heightMap.GetLength (1);
        float topLeftX = (width - 1) / -2f; // centering
        float topLeftZ = (height - 1) / 2f;

        int meshSimplificationIncrement = (levelOfDetail == 0) ? 1 : levelOfDetail * 2; // Possible values will be 1,2,4,6,8,10,12 for mesh size 240
        int verticesPerRowOrCol = (width - 1) / meshSimplificationIncrement + 1;
        MeshData meshData = new MeshData (verticesPerRowOrCol);
        int currentVertex = 0;
        for (int y = 0; y < height; y += meshSimplificationIncrement) {
            for (int x = 0; x < width; x += meshSimplificationIncrement) {
                float vertexHeight = meshHeightCurve.Evaluate (heightMap [x, y]) * heightMultiplier;
                meshData.vertices [currentVertex] = new Vector3 (topLeftX + x, vertexHeight, topLeftZ - y);
                meshData.uvs [currentVertex] = new Vector2 (x / (float)width, y / (float)height);

                if (y < height - 1 && x < width - 1) {
                    int vertexOnTheRight = currentVertex + 1;
                    int vertexBelow = currentVertex + verticesPerRowOrCol;
                    int vertexBelowOnTheRight = currentVertex + verticesPerRowOrCol + 1;
                    meshData.AddTriangle (currentVertex, vertexBelowOnTheRight, vertexBelow);
                    meshData.AddTriangle (vertexBelowOnTheRight, currentVertex, vertexOnTheRight);
                }
                currentVertex++;
            }
        }

        return meshData;
    }

}

public class MeshData {
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;

    private int triangleIndex;

    public MeshData (int verticesPerRowOrCol) {

        /*
         * https://docs.unity3d.com/Manual/AnatomyofaMesh.html
         * TRIANGLES 
         * Each "triangle" in the `triangles` int[] consists of 3 ints, where each int
         * is the index of a Vector3 in the `vertices` Vector3[].
         * 
         * Two triangles per square unit = 6 points per vertex,
         * except no triangles for vertices on the last column or row
         * [1] [2] [3x]    // 1,5,4, 1,2,5, 2,6,5, 2,3,6
         * [4] [5] [6x]    // 4,8,7, 4,5,8, 5,9,8, 5,6,9
         * [7x] [8x] [9x]
        */
        int trianglesCount = (verticesPerRowOrCol - 1) * (verticesPerRowOrCol - 1) * 6;
        triangles = new int[trianglesCount];

        vertices = new Vector3[verticesPerRowOrCol * verticesPerRowOrCol];
        uvs = new Vector2[verticesPerRowOrCol * verticesPerRowOrCol];
    }

    public void AddTriangle (int a, int b, int c) {
        triangles [triangleIndex] = a;
        triangles [triangleIndex + 1] = b;
        triangles [triangleIndex + 2] = c;
        triangleIndex += 3;
    }

    public Mesh CreateMesh () {
        Mesh mesh = new Mesh ();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals ();
        return mesh;
    }
}
