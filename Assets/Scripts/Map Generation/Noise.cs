﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public static class Noise {

    // NOTE - `persistence` should be between 0 and 1
    // NOTE - `lacunarity` should be greater than 1
    public static float[,] GenerateNoise (int mapWidth, int mapHeight, float scale, int octaves, 
                                          float persistence, float lacunarity, int seed, Vector2 offset) {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        float noiseMaxHeight = 0f;
        float noiseMinHeight = 0f;
        float halfMapWidth = mapWidth / 2f;
        float halfMapHeight = mapHeight / 2f;
        float amplitude = 1f;
        float frequency = 1f;

        System.Random prng = new System.Random (seed);
        Vector2[] octaveOffsets = new Vector2[octaves];
        for (int i = 0; i < octaves; i++) {
            float x = prng.Next (-100000, 100000) + offset.x;
            float y = prng.Next (-100000, 100000) - offset.y;
            octaveOffsets [i] = new Vector2 (x, y);
        }

        // Generate noise map, applying lacunarity and persisetence.  Resulting 2d array will consist of floats
        // which are NOT scoped between 0 and 1.
        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                amplitude = 1f;
                frequency = 1f;
                float noiseHeight = 0f;

                for (int i = 0; i < octaves; i++) {
                    float sampleX = (x - halfMapWidth + octaveOffsets [i].x) / scale * frequency;
                    float sampleY = (y - halfMapHeight + octaveOffsets [i].y) / scale * frequency;

                    // `perlinValue` should be between -1 and 1 - PerlinNoise is between 0 to 1 by default
                    float perlinValue = Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    // decrease - `persistence` is between 0 and 1
                    // each successive octave has a lesser effect due to this shrinking multiplier
                    amplitude *= persistence;
                    // increase - `lacunarity` is greater than 1
                    // each successive octave increases non-linear variance, as each successive point in perlin
                    // noise is sampled with at a greater interval
                    frequency *= lacunarity;
                }
                if (noiseHeight > noiseMaxHeight) {
                    noiseMaxHeight = noiseHeight;
                } else if (noiseHeight < noiseMinHeight) {
                    noiseMinHeight = noiseHeight;
                }
                noiseMap [x, y] = noiseHeight;
            }
        }

        return NormalizeNoiseMap (noiseMap, noiseMinHeight, noiseMaxHeight);
    }

    // Normalize height map values
    public static float[,] NormalizeNoiseMap (float[,] noiseMap, float minHeight, float maxHeight) {
        for (int y = 0; y < noiseMap.GetLength (1); y++) {
            for (int x = 0; x < noiseMap.GetLength (0); x++) {
                float normalizedHeight = Mathf.InverseLerp (minHeight, maxHeight, noiseMap [x, y]);
                noiseMap [x, y] = Mathf.Clamp (normalizedHeight, 0, int.MaxValue);
            }
        }
        return noiseMap;
    }

}
