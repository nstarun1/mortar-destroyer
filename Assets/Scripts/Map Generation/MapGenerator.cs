﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
//using UnityEditor.VersionControl;

// MORTAR

public class MapGenerator : MonoBehaviour {

    public enum DrawMode {
        NoiseMap,
        ColorMap,
        Mesh,
        Falloff
    }

    public DrawMode drawMode;
    public float noiseScale;
    //    public bool autoUpdate;
    public int octaves;
    [Range (0, 1)]
    public float persistence;
    public float lacunarity;
    public int seed;
    public Vector2 offset;
    public TerrainType[] regions;
    public float heightMultiplier;
    public AnimationCurve meshHeightCurve;
    [Range (1, 6)]
    public const int mapChunkSize = 241;
    public bool useFalloff;
    public int editorLevelOfDetail;
    [HideInInspector]
	public Color[] colorMap;
    public List<int> playerStartRegionIndices;
	[HideInInspector]

    private float[,] falloffMap;

    public void Awake () {
        MapDisplay display = FindObjectOfType<MapDisplay> ();
        display.CleanMeshColliders ();
        falloffMap = FalloffGenerator.GenerateFalloffMap (mapChunkSize);
    }






    public void DrawMap () {
        MapDisplay display = FindObjectOfType<MapDisplay> ();
        MapData mapData = GenerateMapData (Vector2.zero);

        if (drawMode == DrawMode.NoiseMap) {
            Texture2D texture = TextureGenerator.TextureFromHeightMap (mapData.noiseMap);
            display.DrawTexture (texture);
        } else if (drawMode == DrawMode.ColorMap) {
            GenerateColorMap (mapData.noiseMap);
            Texture2D texture = TextureGenerator.TextureFromColorMap (colorMap, mapChunkSize, mapChunkSize);
            display.DrawTexture (texture);
        } else if (drawMode == DrawMode.Mesh) {
            GenerateColorMap (mapData.noiseMap);
            Texture2D texture = TextureGenerator.TextureFromColorMap (colorMap, mapChunkSize, mapChunkSize);
            MeshData meshData = MeshGenerator.GenerateMesh (mapData.noiseMap, heightMultiplier, meshHeightCurve, editorLevelOfDetail);
            display.DrawMesh (meshData, texture);
        } else if (drawMode == DrawMode.Falloff) {
            display.DrawTexture (TextureGenerator.TextureFromHeightMap (FalloffGenerator.GenerateFalloffMap (mapChunkSize)));
        }
    }

    private MapData GenerateMapData (Vector2 center) {
        float[,] noiseMap = Noise.GenerateNoise (
                                mapChunkSize, 
                                mapChunkSize, 
                                noiseScale, 
                                octaves, 
                                persistence, 
                                lacunarity, 
                                seed, 
                                offset + center
                            );

        GenerateColorMap (noiseMap);
        return new MapData (noiseMap, colorMap);
    }

    // Generates a color map from `noiseMap`.
    // IMPORTANT - relies on `regions` being sorted from low -> high heights.
    private void GenerateColorMap (float[,] noiseMap) {
        colorMap = new Color[mapChunkSize * mapChunkSize];
        for (int y = 0; y < mapChunkSize; y++) {
            for (int x = 0; x < mapChunkSize; x++) {
                if (useFalloff) {
                    noiseMap [x, y] = Mathf.Clamp01 (noiseMap [x, y] - falloffMap [x, y]);
                }

                float currentPosition = noiseMap [x, y];
                for (int i = 0; i < regions.Length; i++) {
                    if (currentPosition >= regions [i].height) {
                        colorMap [y * mapChunkSize + x] = regions [i].color;
                    } else {
                        break;
                    }
                }
            }
        }
    }

    // This is called whenever the public values are changed in the editor.
    private void OnValidate () {
        if (octaves < 1) {
            octaves = 1;
        }
        if (lacunarity < 1f) {
            lacunarity = 1f;
        }
        if (heightMultiplier <= 0f) {
            heightMultiplier = 1;
        }

        falloffMap = FalloffGenerator.GenerateFalloffMap (mapChunkSize);
    }

}

[System.Serializable]
public struct TerrainType {
    public string name;
    public float height;
    public Color color;
    public bool canInitPlayer;
}

public struct MapData {
    public readonly float[,] noiseMap;
    public readonly Color[] colorMap;

    public MapData (float[,] noiseMap, Color[] colorMap) {
        this.noiseMap = noiseMap;
        this.colorMap = colorMap;
    }
}
