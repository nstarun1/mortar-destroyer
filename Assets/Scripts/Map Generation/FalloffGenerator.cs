﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// MORTAR

public static class FalloffGenerator {

    public static float[,] GenerateFalloffMap (int size) {
        float[,] map = new float[size, size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                float x = i / (float)size * 2f - 1f;
                float y = j / (float)size * 2f - 1f;
                float val = Mathf.Max (Math.Abs (x), Math.Abs (y));
                map [i, j] = Evaluate (val);
            }
        }

        return map;
    }

    private static float Evaluate (float value) {
        float a = 3;
        float b = 10.2f;

        return Mathf.Pow (value, a) / (Mathf.Pow (value, a) + Mathf.Pow (b - b * value, a));
    }

}
