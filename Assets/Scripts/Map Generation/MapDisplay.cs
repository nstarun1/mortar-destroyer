﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;

public class MapDisplay : MonoBehaviour {

    public Renderer textureRenderer;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public Texture2D tex;
	[HideInInspector]

    // Set texture on textureRenderer's `sharedMaterial`, since `material` is only available at runtime.
    // TODO - explore how to do this at runtime via UI menu!
    public void DrawTexture (Texture2D texture) {
        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3 (texture.width, 1, texture.height);
    }

    public void DrawMesh (MeshData meshData, Texture2D texture) {
		
        tex = texture;
        meshFilter.sharedMesh = meshData.CreateMesh ();
        meshFilter.gameObject.AddComponent (typeof(MeshCollider));
        meshRenderer.sharedMaterial.mainTexture = texture;


	}

    public void CleanMeshColliders () {
        MeshCollider[] colliders = meshFilter.gameObject.GetComponents<MeshCollider> ();
        for (var i = 0; i < colliders.Length; i++) {
            Destroy (colliders [i]);
        }
    }

    public void OnApplicationQuit () {
        CleanMeshColliders ();
    }

}