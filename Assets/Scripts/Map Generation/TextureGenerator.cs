﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Mortar

public static class TextureGenerator {

    public static Texture2D TextureFromColorMap (Color[] colorMap, int width, int height) {
        Texture2D texture = new Texture2D (width, height);
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.SetPixels (colorMap);
        texture.Apply ();
        return texture;
    }

    public static Texture2D TextureFromHeightMap (float[,] noiseMap) {
        int width = noiseMap.GetLength (0);
        int height = noiseMap.GetLength (1);
        Color[] shades = HeightMapShades (width, height, noiseMap);
        return TextureFromColorMap (shades, width, height);
    }

    public static Color[] HeightMapShades (int width, int height, float[,] noiseMap) {
        Color[] colors = new Color[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                colors [y * width + x] = Color.Lerp (Color.black, Color.white, noiseMap [x, y]);
            }
        }
        return colors;
    }

}
