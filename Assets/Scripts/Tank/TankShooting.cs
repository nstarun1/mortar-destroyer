﻿using UnityEngine;
using UnityEngine.UI;

namespace Complete
{
	public class TankShooting : MonoBehaviour
	{


		//public int this.name = 1;
		public Camera mainCam;
		public Camera gunCam;
		public Slider launchPower;

		public GameObject gunCamView;
		Camera bulletCam;
		GameObject enemy;
		// Used to identify the different players.
		public Rigidbody m_Shell;
		// Prefab of the shell.
		public Transform m_FireTransform;
		// A child of the tank where the shells are spawned.
		//public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
		public AudioSource m_ShootingAudio;
		// Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
		//public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
//		public AudioClip m_FireClip;
		// Audio that plays when each shot is fired.
		// The force given to the shell if the fire button is not held.
		public float m_MaxLaunchForce = 30f;
		// The force given to the shell if the fire button is held for the max charge time.
		public float m_MinLaunchForce = 0f;        // The force given to the shell if the fire button is not held.
		public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.

		private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
		private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
		private bool m_Fired;                       // Whether or not the shell has been launched with this button press.

		private float triggerValue;

		private string m_FireButton;
		// The input axis that is used for launching shells.
		private string m_FireButtonXBox360;
		// Whether or not the shell has been launched with this button press.

		private string m_CameraButton;
		Rigidbody shellInstance;

		private float _fireRate = 2;
		private float _nextFireTime = 0;
		RenderTexture contents;
		private void OnEnable ()
		{
			m_CurrentLaunchForce = m_MinLaunchForce;

		}


		private void Start ()
		{	

			Vector2 size = gunCamView.GetComponent<RectTransform>().rect.size;
						
			contents= new RenderTexture((int) size.x, (int) size.y, 32, RenderTextureFormat.ARGB32);
			gunCam.aspect = size.x / size.y;
			gunCam.targetTexture = contents;
			gunCamView.GetComponent<RawImage>().texture = contents;

			// The fire axis is based on the player number.
				
			if (SystemInfo.operatingSystem.StartsWith ("Win")) {

				m_FireButton = "Fire" + this.name;
			} else {

				m_FireButton = "XBox360_Fire_" + this.name;
			}
			m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
		}

		private void MonitorTrigger(){



		}


		private void Update ()
		{
			if (!GetComponent<TankHealth>().IsAlive())
				return;
			
			launchPower.value = m_CurrentLaunchForce / m_MaxLaunchForce;
			triggerValue = Input.GetAxis(m_FireButton);
			
			if (triggerValue == 0f || Time.time<=_nextFireTime)
			{
				return;
			}

			if (enemy && bulletCam ) {

				bulletCam.transform.LookAt (enemy.transform.position);

			}


			if (shellInstance == null && gunCam.targetTexture != contents) {

				gunCam.targetTexture = contents;
			}

				

			// The slider should have a default value of the minimum launch force.
			// If the max force has been exceeded and the shell hasn't yet been launched...
			if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
			{
				// ... use the max force and launch the shell.
				m_CurrentLaunchForce = m_MaxLaunchForce;
				Fire ();
			}
			// Otherwise, if the fire button has just started being pressed...

			else if (m_Fired)
			{
				// ... reset the fired flag and reset the launch force.
				m_Fired = false;
				m_CurrentLaunchForce = m_MinLaunchForce;

				// Change the clip to the charging clip and start it playing.
			
			}
			// Otherwise, if the fire button is being held and the shell hasn't been launched yet...
			else if (triggerValue > -1 && !m_Fired)
			{
				// Increment the launch force and update the slider.
				m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

			}
			// Otherwise, if the fire button is released and the shell hasn't been launched yet...
			else if (triggerValue == -1 && !m_Fired && m_CurrentLaunchForce > 0)
			{
				// ... launch the shell.
				
				Fire ();
			}
		}






		private void Fire ()
		{

			// Set the fired flag so only Fire is only called once.
			m_Fired = true;

			// Create an instance of the shell and store a reference to it's rigidbody.
			shellInstance =
				Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

			shellInstance.gameObject.transform.parent = this.gameObject.transform;


			//shellInstance.gameObject.GetComponentInChildren<Camera> ().rect = new Rect (.01f, miniMap.anchorMin.y, .2f, .2f);


			//shellInstance.transform.parent = transform;

			bulletCam=shellInstance.gameObject.GetComponentInChildren<Camera> ();


			bulletCam.aspect = 1f;

			bulletCam.targetTexture = contents;


			gunCamView.GetComponent<RawImage>().texture = contents;


			enemy = enemyTarget ();











			// Set the shell's velocity to the launch force in the fire position's forward direction.
			shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward; 
			//recoil force
			GetComponent<Rigidbody> ().AddForce (-m_CurrentLaunchForce * m_FireTransform.forward * 5);

			// Change the clip to the firing clip and play it.

			m_ShootingAudio.Play ();


			_nextFireTime = Time.time + 1 / _fireRate;


		}

		public bool IsVisibleFrom(Renderer renderer, Camera camera)
		{

			Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
			try{
				return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);}
			catch {return false;
			}
		}




		GameObject enemyTarget(){
		


			foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Tank")) {

				if (IsVisibleFrom (enemy.GetComponentInChildren<Renderer> (), mainCam) && this.gameObject.transform.root.gameObject.name!=enemy.name) {
					return enemy;

				}

			} return null;
			}
				
		
		




	}
}
