﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//namespace Complete
//{
//    public class Tank : MonoBehaviour
//    {
//        [SerializeField] private Camera _camera;
//        [SerializeField] private RectTransform _miniMapTransform;
//        
//        [SerializeField] private MeshRenderer[] _tankMeshes;
//        [SerializeField] private TankMovement _tankMovement;
//        [SerializeField] private TankShooting _tankShooting;
//
//        [SerializeField] private MiniMapMarker _miniMapMarker;
//        [SerializeField] private MiniMapController _miniMapController;
//
//        public void Configure(int totalPlayers, int playerNumber, Vector3 position, Color color)
//        {
//            name = "Player " + playerNumber;
//            transform.position = position;
//			_miniMapMarker._color = color;
//			_miniMapMarker.ConfigureSize ();
//
//
//            _tankMovement.m_PlayerNumber = playerNumber;
//            _tankShooting.m_PlayerNumber = playerNumber;
//
//		
//
//            for(int i = 0; i < _tankMeshes.Length; i++)
//			{	
//                _tankMeshes[i].material.color = color;
//            }
//
//
//
//
//
//            // TODO: Improve.
//            if (totalPlayers == 2)
//            {
//                if (playerNumber == 1)
//                {
//                    _camera.rect = new Rect(0, 0.5f, 1f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.825f, 0.525f);
//                    _miniMapTransform.anchorMax = new Vector2(0.975f, 0.675f);
//                }
//                else if (playerNumber == 2)
//                {
//                    _camera.rect = new Rect(0, 0, 1f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.825f, 0.025f);
//                    _miniMapTransform.anchorMax = new Vector2(0.975f, 0.175f);
//                }
//            }
//            else if (totalPlayers == 3)
//            {
//                if (playerNumber == 1)
//                {
//                    _camera.rect = new Rect(0, 0.5f, 0.5f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.375f, 0.525f);
//                    _miniMapTransform.anchorMax = new Vector2(0.475f, 0.625f);
//                }
//                else if (playerNumber == 2)
//                {
//                    _camera.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.875f, 0.525f);
//                    _miniMapTransform.anchorMax = new Vector2(0.975f, 0.625f);
//                }
//                else if (playerNumber == 3)
//                {
//                    _camera.rect = new Rect(0f, 0f, 1f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.875f, 0.025f);
//                    _miniMapTransform.anchorMax = new Vector2(0.975f, 0.125f);
//                }
//            }
//            else if (totalPlayers == 4)
//            {
//                if (playerNumber == 1)
//                {
//                    _camera.rect = new Rect(0, 0.5f, 0.5f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.375f, 0.525f);
//                    _miniMapTransform.anchorMax = new Vector2(0.475f, 0.625f);
//                }
//                else if (playerNumber == 2)
//                {
//                    _camera.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.875f, 0.525f);
//                    _miniMapTransform.anchorMax = new Vector2(0.975f, 0.625f);
//                }
//                else if (playerNumber == 3)
//                {
//                    _camera.rect = new Rect(0f, 0f, 0.5f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.375f, 0.025f);
//                    _miniMapTransform.anchorMax = new Vector2(0.475f, 0.125f);
//                }
//                else if (playerNumber == 4)
//                {
//                    _camera.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
//                    
//                    _miniMapTransform.anchorMin = new Vector2(0.875f, 0.025f);
//                    _miniMapTransform.anchorMax = new Vector2(0.975f, 0.125f);
//                }
//            }
//        }
//
//        public void Reconfigure()
//        {
//            _miniMapController.FindPointsOfInterest();
//        }
//    }
//}
