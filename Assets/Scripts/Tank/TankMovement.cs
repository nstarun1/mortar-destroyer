﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Complete
{
	public class TankMovement : MonoBehaviour
	{
		//public int this.name ;
		// Used to identify which tank belongs to which player.  This is set by this tank's manager.
		//public float m_Speed = 12f;
		// How fast the tank moves forward and back.
		public float m_TurnSpeed = 180f;
		// How fast the tank turns in degrees per second.
		public AudioSource m_MovementAudio;
		// Reference to the audio source used to play engine sounds. NB: different to the shooting audio source.
		public AudioClip m_EngineIdling;
		// Audio to play when the tank isn't moving.
		public AudioClip m_EngineDriving;
		// Audio to play when the tank is moving.
		public float m_PitchRange = 0.2f;
		// The amount by which the pitch of the engine noises can vary.
		public float maxSpeed;
		public Collider myCollider;
		public AudioClip m_bumping;
		public int m_PlayerNumber;

        
		private string m_MovementAxisName;
		// The name of the input axis for moving forward and back.
		private string m_MovementAxisNameXBox360;
		// The name of the input axis for moving forward and back.
		private string m_TurnAxisName;
		// The name of the input axis for turning.
		private string m_TurnAxisNameXBox360;
		// The name of the input axis for turning.
		private Rigidbody m_Rigidbody;
		// Reference used to move the tank.
		private float m_MovementInputValue;
		// The current value of the movement input.
		private float m_TurnInputValue;
		// The current value of the turn input.
		private float m_OriginalPitch;
		// The pitch of the audio source at the start of the scene.
        
		public Rigidbody tankRB;
		public Transform centerOfMass;

		public float t_VertSpeed = 20f;
		// How fast the turret rises and lowers.
		public float t_TurnSpeed = 270f;
		// How fast the turret turns in degrees per second.
		public float e1;
		public float e2;



		private string t_TurnAxisName;
		// The name of the input axis for turning the turret
		private string t_TurnAxisNameXBox360;
		// The name of the input axis for turning the turret
		private string t_VertAxisName;
		// The name of the input axis for raising the turret
		private string t_VertAxisNameXBox360;
		// The name of the input axis for raising the turret
		private Transform t_transform;
		// Reference turret rigidbody
		private float t_TurnInputValue;
		// The current value of the turn input.
		private float t_VerticalInputValue;
		// The current value of the vertical input.
		private GameObject turret;

		private void Start ()
		{			
			
			if (SystemInfo.operatingSystem.StartsWith ("Win")) {
				m_MovementAxisName = "Vertical" + this.name;
			
				m_TurnAxisName = "Horizontal" + this.name;
				t_VertAxisName = "TurretUpDown" + this.name;
				t_TurnAxisName = "TurretTurn" + this.name;
			} else {
				
				m_MovementAxisName = "XBox360_Vertical_" + this.name;
				m_TurnAxisName = "XBox360_Horizontal_" + this.name;
				t_VertAxisName = "XBox360_TurretVertical_" + this.name;
				t_TurnAxisName = "XBox360_TurretTurn_" + this.name;
			}
			t_transform = getTurret ();


		//	print (m_MovementAxisName + " " + this.transform.root.gameObject.name );
			tankRB.centerOfMass = centerOfMass.localPosition;
			m_Rigidbody.maxAngularVelocity = 2.0f;

			// Store the original pitch of the audio source.
			m_OriginalPitch = m_MovementAudio.pitch;
		}


	







		private ParticleSystem[] m_particleSystems;
		// References to all the particles systems used by the Tanks

		private void Awake ()
		{


			m_Rigidbody = GetComponent<Rigidbody> ();

	

		}


		private Transform getTurret ()
		{

			foreach (Transform t in GetComponentsInChildren<Transform> ()) {
			
				if (t.name == "TankTurret") {

					return t;
				
				}

			}

			return null;

		}




		private void OnEnable ()
		{
			// When the tank is turned on, make sure it's not kinematic.
			m_Rigidbody.isKinematic = false;

			// Also reset the input values.
			m_MovementInputValue = 0f;
			m_TurnInputValue = 0f;


			// We grab all the Particle systems child of that Tank to be able to Stop/Play them on Deactivate/Activate
			// It is needed because we move the Tank when spawning it, and if the Particle System is playing while we do that
			// it "think" it move from (0,0,0) to the spawn point, creating a huge trail of smoke
			m_particleSystems = GetComponentsInChildren<ParticleSystem> ();
			for (int i = 0; i < m_particleSystems.Length; ++i) {
				m_particleSystems [i].Play ();
			}
		}


		private void OnDisable ()
		{
			// When the tank is turned off, set it to kinematic so it stops moving.
			m_Rigidbody.isKinematic = true;

			// Stop all particle system so it "reset" it's position to the actual one instead of thinking we moved when spawning
			for (int i = 0; i < m_particleSystems.Length; ++i) {
				m_particleSystems [i].Stop ();
			}
		}


      

		private void Update ()
		{
			// Store the value of both input axes.

			try {
				m_MovementInputValue = Input.GetAxis (m_MovementAxisName);
				m_TurnInputValue = Input.GetAxis (m_TurnAxisName);
				t_VerticalInputValue = Input.GetAxis (t_VertAxisName);
				t_TurnInputValue = Input.GetAxis (t_TurnAxisName);
			} catch {
			}
		        


				EngineAudio ();
			
		}





		private void EngineAudio ()
		{
			// If there is no input (the tank is stationary)...
			if (Mathf.Abs (m_MovementInputValue) < 0.1f && Mathf.Abs (m_TurnInputValue) < 0.1f) {
				// ... and if the audio source is currently playing the driving clip...
				if (m_MovementAudio.clip == m_EngineDriving || m_MovementAudio.clip == m_bumping) {
					// ... change the clip to idling and play it.
					m_MovementAudio.clip = m_EngineIdling;
					m_MovementAudio.pitch = Random.Range (m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
					m_MovementAudio.Play ();
				}
			} else {
				// Otherwise if the tank is moving and if the idling clip is currently playing...
				if (m_MovementAudio.clip == m_EngineIdling) {
					// ... change the clip to driving and play.
					m_MovementAudio.clip = m_EngineDriving;
					m_MovementAudio.pitch = Random.Range (m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
					m_MovementAudio.Play ();




				}
			}
		}


		private void FixedUpdate ()
		{
			// Adjust the rigidbodies position and orientation in FixedUpdate.
			Move ();
			Turn ();

			if (t_VerticalInputValue != 0f) {
				RaiseLowerTurret ();
			}
			if (t_TurnInputValue != 0) {
				TurnTurret ();
			}

			//print ("Current Bearing " + transform.rotation.eulerAngles.y);


//			GameObject.Find ("SliderBearingAngle").GetComponent<Slider> ().value = t_transform.eulerAngles.y;
//			//GameObject.Find("BearingAngleText").GetComponent<Text>().text =t_transform.eulerAngles.y.ToString("F0");
//
//			GameObject.Find ("SliderGroundAngle").GetComponent<Slider> ().value = t_transform.eulerAngles.x;
//			GameObject.Find("GroundAngleText").GetComponent<Text>().text =(360f-t_transform.eulerAngles.x).ToString("F0");

		}


		private void RaiseLowerTurret ()
		{

			// Determine the number of degrees to be turned based on the input, speed and time between frames.
			float vert = t_VerticalInputValue * t_VertSpeed * Time.deltaTime;
		
			t_transform.RotateAround (t_transform.position, t_transform.right, vert);

		}

		private void TurnTurret ()
		{

			// Determine the number of degrees to be turned based on the input, speed and time between frames.
			float turn = t_TurnInputValue * t_TurnSpeed * Time.deltaTime;





			t_transform.RotateAround (t_transform.position, m_Rigidbody.transform.up, turn);

//			(Vector3.up, turn,Space.World);


		}



		private void Move ()
		{
			// Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
			//Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;


			// Apply this movement to the rigidbody's position.
			//m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
        	
			if (m_Rigidbody.velocity.magnitude < maxSpeed) {
				//print ("applying power");
				m_Rigidbody.AddForce (transform.forward * 30 * m_MovementInputValue);
			}



		
		}


		void OnCollisionEnter (Collision collision)
		{


			if (collision.collider.name == "Mesh") {
				if (Mathf.Abs (m_MovementInputValue) > 0.1f || Mathf.Abs (m_TurnInputValue) > 0.1f) {
					StartCoroutine (bump (collision.impulse.magnitude));
				}
			}

		}




		IEnumerator bump (float volume)
		{
			//print (volume);


			m_MovementAudio.clip = m_bumping;

			m_MovementAudio.volume = volume;

			m_MovementAudio.Play ();

			yield return new WaitForSeconds (1);


		}

	





		private void Turn ()
		{
////             Determine the number of degrees to be turned based on the input, speed and time between frames.
//            float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;
//
////             Make this into a rotation in the y axis.
//            Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);
//
////             Apply this rotation to the rigidbody's rotation.
//            m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
				
			m_Rigidbody.AddTorque (transform.up * m_TurnInputValue * 20);


		}
	}
}