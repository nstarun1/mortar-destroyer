﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;
using System.Linq;
public class FlagGameController : MonoBehaviour
{
	public DeathMatchGameMode deathMatchGameMode;
	public float respawnRate;
	public float tankHitpoints;
	int teamNumber;
	GameObject newTank;
	Vector2 aMax;
	Vector2 aMin;
	public GameObject flag;

	public List<Transform> tankPositions;
	public Dictionary <string,int> team=new Dictionary<string, int>();
	List <GameObject> tanks = new List <GameObject> ();
	public List <int> tails;
	public List<int> shuffled;
	public GameObject tankPrefab;
	GameObject player;
	int totalTanks;
	public Initiate init;
	List <string> camoNames = new List<string> ();
	Dictionary<int,Color> teamColors = new Dictionary<int, Color> ();
	public Dictionary<string,int> teamAssign = new Dictionary<string, int> ();

	List <int> shuffled_tails(List<int> n){
	List <int> shuffled = new List<int> ();
	int ct = 0;
	while (n.Count > 0) {
			if (ct > 10) {
				return shuffled   ;
			}

			int random_index = Random.Range (0, n.Count - 1);
			shuffled.Add (n [random_index]);
			n.RemoveAt (random_index);

			ct++;

		}
		return shuffled;
				
		}

	Vector3 position;
	string name;
	Rect camRect;
	RectTransform playerCanvas;

	void FixedUpdate(){
		
		if (tanks.Count > 0) {
			foreach (GameObject tank in tanks) {
				if (!tank.GetComponent<TankData> ().alive) {
					position = tank.transform.position;
					name = tank.name;
					teamNumber = tank.GetComponent<TankData> ().teamNumber;

					camRect = tank.GetComponentInChildren<ConfigureMainCamera> ().mainCam.rect;

					tank.transform.Find("GUICanvas").gameObject.SetActive(true);
					playerCanvas = tank.GetComponentInChildren<ConfigurePlayerCanvas> ().playerCanvas;

					aMin = playerCanvas.anchorMin;
					aMax = playerCanvas.anchorMax;
					tank.transform.Find("GUICanvas").gameObject.SetActive(false);

					//tank.GetComponentInChildren<FlagGameController> ().gameObject.transform.parent = null;
					transform.parent = null;

					Vector3 flagPosition = transform.position;
					flagPosition.y -= 1f;

					transform.position = flagPosition;


					//return;
				


					Destroy (tank,20f);

					tanks.Remove (tank);
				



					StartCoroutine (spawnOne(name, teamNumber, camRect, aMin, aMax));





//					GameObject pirateFlag = Instantiate (flag);
//					pirateFlag.transform.position = position;
//					pirateFlag.transform.Rotate (0, 0, 0);
//
//					pirateFlag.name = "pirateFlag";
					break;
				}

			
	
			}
		}
	}


	IEnumerator spawnOne(string name, int teamNumber, Rect camRect, Vector2 aMin, Vector2 aMax){
	
		yield return new WaitForSeconds (respawnRate);




		newTank=Instantiate (tankPrefab);
		newTank.name = name;
		newTank.GetComponent<TankData> ().teamNumber = teamNumber;
		newTank.transform.position = position + new Vector3 (2, 0, 3);	


		newTank.GetComponentInChildren<ConfigureMainCamera> ().mainCam.rect = camRect;	
		RectTransform newPlayerCanvas = newTank.GetComponentInChildren<ConfigurePlayerCanvas> ().playerCanvas;
		newPlayerCanvas.anchorMin = aMin;
		newPlayerCanvas.anchorMax = aMax;

		setColor (newTank, teamNumber);
		newTank.GetComponent<TankHealth>().hitPoints = tankHitpoints;

		tanks.Add (newTank);

	}

	void OnTriggerEnter(Collider other){

		if (other.name == "Target") {
			if (deathMatchGameMode)
			{
				deathMatchGameMode.ShowWinner(this.transform.root.gameObject);
			}
		}
		if (other.gameObject.tag == "Tank" && other.gameObject.GetComponent<TankHealth>().IsAlive()) {

			foreach (Transform t in other.GetComponentsInChildren<Transform>()) {
		

				if (t.name == "TankTurret") {

					transform.parent = t;

					transform.localPosition = new Vector3 (.2f, .6f, -.5f);
						}
			
				}

		
		}

	}

	// Use this for initialization
	void Start ()
	{
		teamColors [0] = Color.green;
		teamColors [1] = Color.red;
		camoNames.Add ("_CamoBlackTint");			
		camoNames.Add ("_CamoRedTint");
		camoNames.Add ("_CamoGreenTint");
		camoNames.Add ("_CamoBlueTint");	

		tails = new List  <int> () { 0, 1, 0, 1 };
		shuffled = shuffled_tails (tails);

		totalTanks=GameObject.Find ("SpawnTank").GetComponent<SpawnTanks> ()._totalPlayers;
	
		StartCoroutine (Reposition ());


		}


	IEnumerator Reposition(){

		while (!init.spawnComplete) {
			yield return new WaitForSeconds (.2f);}


		tanks = GameObject.Find ("SpawnTank").GetComponent<SpawnTanks> ()._tanks;



		for (int z = 0; z < totalTanks; z++) {

			player = tanks [z];
			teamAssign [player.name] = shuffled [z];

			TankData data = player.AddComponent<TankData> ();

			
			data.teamNumber = shuffled [z];


			player.transform.position = tankPositions [z].position;

			setColor (player, data.teamNumber);

			player.transform.LookAt (transform);
			player.SetActive (true);

			player.GetComponent<TankHealth>().hitPoints = tankHitpoints;
			player.GetComponent<TankHealth>().startHitpoints = tankHitpoints;


//			SetPlayerWindows (tanks);
		}
	}

		



	public void  SetPlayerWindows (List<GameObject>  _tanks)
	{	
		int columns = 2;
		int rows = 2;


		float width = 1f / columns;
		float height = 1f / rows;

		int i = 0;
		for (int c = 0; c < columns; c++) {
			for (int r = 0; r < rows; r++) {


				float tankX = c * width;
				float tankY = r * height;
								_tanks [i].GetComponentInChildren<ConfigureMainCamera> ().mainCam.rect = new Rect (tankX, tankY, width, height);

				RectTransform playerCanvas = _tanks [i].GetComponentInChildren<ConfigurePlayerCanvas> ().playerCanvas;

				playerCanvas.anchorMin = new Vector2 (tankX, tankY);
				playerCanvas.anchorMax = new Vector2 (tankX + width, tankY + height);

				i++;
				if (i > _tanks.Count - 1) {
					break;
				}

			}

		} 
}


	void setColor(GameObject player,int teamNumber){
		foreach (string camoType in camoNames) {
			Vector4 basic = teamColors [teamNumber];

			foreach (MeshRenderer r in player.GetComponentsInChildren<MeshRenderer>()) {
				foreach (Material m in r.materials) {

					if (m.HasProperty ("_CamoBlackTint")) {
						m.SetVector (camoType, basic + new Vector4 (Random.value - .5f, Random.value - .5f, Random.value - .5f, 0) * .1f);

					}
				}	
			}

		}
	
	}

}




