﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reticle : MonoBehaviour {
	public float maxRange=20	;
	Transform fireControl;
	Ray fireLine;
	RaycastHit hit;
	public LineRenderer line;
	public Transform reticle;
	float distanceHit;


	// Use this for initialization
	void Start () {
	
	

	}
	
	// Update is called once per frame
	void Update () {
		
		fireLine = new Ray (transform.position, transform.forward);
		if (Physics.Raycast (fireLine, out hit,maxRange)) {


			distanceHit=(hit.point - transform.position).magnitude;
			reticle.localPosition = new Vector3 (0, 0, Mathf.Min(maxRange,distanceHit-1));



		} else {
			reticle.localPosition = new Vector3 (0, 0, maxRange);

		}




	}
}
