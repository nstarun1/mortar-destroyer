﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigureMiniMapAnchors : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetAnchors(Vector2 anchorMin, Vector2 anchorMax){
	
		GetComponent<RectTransform> ().anchorMin = anchorMin;
		GetComponent<RectTransform> ().anchorMax = anchorMax;


	}


}
