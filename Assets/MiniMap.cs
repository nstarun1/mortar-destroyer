﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 myPosition = GetComponentInParent<Transform> ().position;
		Vector3 enemyPosition = GameObject.Find ("Enemy").GetComponent<Transform> ().position;
		Vector3 vectorBetween =enemyPosition-myPosition;
		Vector3 midPoint = vectorBetween / 2f;
		float distance = vectorBetween.magnitude;



		GetComponent<Camera> ().orthographicSize = distance * 1.1f;
		GetComponent<Camera> ().transform.position= midPoint +new Vector3(0,50,0);

}
}