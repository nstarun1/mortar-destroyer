﻿using UnityEngine;

namespace Complete
{
	public class TurretMovement : MonoBehaviour
	{
		public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player.  This is set by this tank's manager.
		public float t_VertSpeed = 20f;                 // How fast the turret rises and lowers.
		public float t_TurnSpeed = 270f;            // How fast the turret turns in degrees per second.

		private string t_TurnAxisName;          // The name of the input axis for turning the turret
		private string t_VertAxisName;              // The name of the input axis for raising the turret
		private Rigidbody t_Rigidbody;              // Reference turret rigidbody
		private float t_TurnInputValue;         // The current value of the turn input.
		private float t_VerticalInputValue;             // The current value of the vertical input.



		private void Awake ()
		{
			t_Rigidbody = GetComponent<Rigidbody> ();
		}


		private void OnEnable ()
		{
			// When the tank is turned on, make sure it's not kinematic.
			//t_Rigidbody.isKinematic = false;

			// Also reset the input values.
			t_TurnInputValue = 0f;
			t_VerticalInputValue = 0f;

		}


		private void OnDisable ()
		{
			// When the tank is turned off, set it to kinematic so it stops moving.
			//t_Rigidbody.isKinematic = true;

		}


		private void Start ()
		{
			// The axes names are based on player number.
			t_VertAxisName = "TurretVertical" + m_PlayerNumber;
			t_TurnAxisName = "TurretTurn" + m_PlayerNumber;

		}

		private void Update ()
		{
			// Store the value of both input axes.
			t_TurnInputValue = Input.GetAxis (t_TurnAxisName);
			t_VerticalInputValue = Input.GetAxis (t_VertAxisName);

		}


		private void FixedUpdate ()
		{
			// Adjust the rigidbodies position and orientation in FixedUpdate.
			TurretMoveVertical ();

			Turn ();


		}


		private void TurretMoveVertical ()
		{
			// Determine the number of degrees to be turned based on the input, speed and time between frames.
			float turn = t_VerticalInputValue * t_VertSpeed * Time.deltaTime;
//

			// Make this into a rotation in the x axis.
			Quaternion turnRotation = Quaternion.Euler (turn,0, 0);

	

			// Apply this rotation to the rigidbody's rotation.
			t_Rigidbody.MoveRotation (t_Rigidbody.rotation * turnRotation);		}





		private void Turn ()
		{
			// Determine the number of degrees to be turned based on the input, speed and time between frames.
			float turn = t_TurnInputValue * t_TurnSpeed * Time.deltaTime;
			//print (turn);
			// Make this into a rotation in the y axis.
			Quaternion turnRotation = Quaternion.Euler (0, turn, 0f);

			// Apply this rotation to the rigidbody's rotation.
			t_Rigidbody.MoveRotation (t_Rigidbody.rotation * turnRotation);




		}
	}



}

