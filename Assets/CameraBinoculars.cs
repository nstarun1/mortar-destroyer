﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Complete{
public class CameraBinoculars : MonoBehaviour {
	public GameObject tank;
		public Transform gunTip;
	public Camera gunCam;

	int tankNo;	
	float zoomInput;
	Vector3 gunCamStartPosition;
	Vector3 camPos;
	public float zoomSpeed;

	private float maxZoom = 55;
	// Use this for initialization
	RaycastHit hit;

	private string _zoomInput;
	private string _zoomInputXBox360;

	private float currentZoom;
	private float initialFov;
	
	void Start () {
		tankNo = tank.GetComponent<TankMovement> ().m_PlayerNumber;	
		
		gunCamStartPosition= transform.localPosition;
		initialFov = gunCam.fieldOfView;

		currentZoom = initialFov;

		_zoomInput = "Zoom" + this.transform.root.name;
		_zoomInputXBox360 = "XBox360_Zoom_" + this.transform.root.name;
	}
	
	// Update is called once per frame
	void Update () {
			
//			Physics.Raycast(gunTip.position,gunTip.forward,out hit);
//			maxZoom = (hit.point - gunTip.position).magnitude-1;
//			print (maxZoom);
//			l.useWorldSpace = true;
		


			try {
				zoomInput = Input.GetAxis (_zoomInputXBox360);
			} catch (Exception) {
				zoomInput = Input.GetAxis (_zoomInput);
			}
									
			if (zoomInput>0f ) {
				//gunCam.enabled = true;
//				float currZ = Mathf.Clamp ( transform.localPosition.z+ zoomInput * zoomSpeed * Time.deltaTime, -.5f, maxZoom);
//				transform.localPosition =  new Vector3 (0, 0, currZ);

				currentZoom = Mathf.Clamp(currentZoom - 10, initialFov - maxZoom, initialFov);

			} else {
				
//				float currZ = Mathf.Clamp ( transform.localPosition.z -  zoomSpeed * Time.deltaTime, -.5f, maxZoom);
//				transform.localPosition =  new Vector3 (0, 0, currZ);

				currentZoom = Mathf.Clamp(currentZoom + 10, initialFov - maxZoom, initialFov);



				//	transform.localPosition = gunCamStartPosition;
			}
				
		gunCam.fieldOfView = currentZoom;





		//print (zoom + ":" + currZ);






	}
}
}