﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;
using UnityEngine.SceneManagement;

public class Utilities : MonoBehaviour
{
    public int Players;

    private KeyCode _restartLevelButton = KeyCode.Joystick1Button9;
    private KeyCode _levelSelectButton = KeyCode.Joystick1Button10;


    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(_levelSelectButton))
        {
            if (SceneManager.GetActiveScene().buildIndex != 0)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }


        if (Input.GetKeyDown(_restartLevelButton))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}