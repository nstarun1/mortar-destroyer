﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSelection : MonoBehaviour
{
	public Image image;
	
	public Color onColor;
	public Color offColor;

	private Dropdown dropDown;

	void Start()
	{
		dropDown = GetComponent<Dropdown>();
	}
	
	public void TurnOn()
	{
		image.color = onColor;
	}

	public void TurnOff()
	{
		image.color = offColor;
	}

	public void Selected()
	{		
		if (GetComponent<OpenWorld>())
		{
			GetComponent<OpenWorld>().OpenScene();
		}
	}

	public bool MoveUp()
	{
		if (dropDown)
		{
			dropDown.value--;

			return true;
		}

		return false;
	}

	public bool MoveDown()
	{
		if (dropDown)
		{
			dropDown.value++;

			return true;
		}

		return false;
	}
}
