﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeShell : MonoBehaviour {
	
	public float blastRadius;
	public GameObject explosion;
	bool explosionComplete=false;
	public AudioSource shellExploding;

	private bool exploded;

	private Vector3 explodedPosition;
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(CheckOffMap());
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 velocity = GetComponent<Rigidbody>().velocity;
		
		if (velocity != Vector3.zero)
		{
			transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
		}
	}






	void OnCollisionEnter(Collision collision){

		if (exploded)
		{
			return;
		}

		GetComponent<Rigidbody>().isKinematic = true;
		exploded = true;
		GameObject exploder=Instantiate (explosion,GetComponent<Transform>());

		GetComponent<MeshRenderer>().enabled = false;

		shellExploding.Play ();


		//Destroy (exploder, 3f);


		if (GetComponentInChildren<ParticleSystem> ()) {
			GetComponentInChildren<ParticleSystem> ().Play ();
		}


		Collider[] colliders = Physics.OverlapSphere (transform.position, blastRadius);


		foreach (Collider hit in colliders) {
			
		
				float blastDistance = (hit.transform.position-transform.position).magnitude;
				float blastVelocity = collision.relativeVelocity.magnitude;
				float blastProximity= 1 - blastDistance / blastRadius;

				float explodeForce = blastProximity * blastVelocity;



			if (hit.gameObject.GetComponent<TankHealth>()){
				hit.gameObject.SendMessage ("CollateralDamage", blastVelocity * blastProximity);

				hit.attachedRigidbody.AddExplosionForce (explodeForce, hit.transform.position, blastDistance, 1);
			}


//								//	(hit.transform.position-collision.transform.position).magnitude/blastRadius*collision.relativeVelocity;
//				print(hit.gameObject.name);
//

		



			}

		explosionComplete = true;



		//transform.DetachChildren ();




		StartCoroutine (WaitToDestroy());


	}


	IEnumerator WaitToDestroy()
	{		
		while (shellExploding.isPlaying || !explosionComplete)
		{
			yield return new WaitForEndOfFrame();
		}
		
		Destroy(gameObject);
	}


	IEnumerator CheckOffMap()
	{		
		while (transform.position.y > -100)
		{
			yield return new WaitForEndOfFrame();
		}
		
		Destroy(gameObject);
	}



}


