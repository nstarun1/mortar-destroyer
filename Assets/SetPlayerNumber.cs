﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class SetPlayerNumber : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Text playersText;

	int numPlayers;

	bool gotFocus=false;
	public void Start(){
		numPlayers = GameObject.Find("DoNotDestroy").GetComponent<Utilities>().Players;
		//InputField.OnChangeEvent();
	}
	public void OnPointerEnter(PointerEventData eventData)
	{
		gotFocus = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		gotFocus = false;
	}

	void Update(){
		
		if (gotFocus) {


			if (Input.GetKey (KeyCode.Alpha1) || Input.GetKey (KeyCode.Alpha2) || Input.GetKey (KeyCode.Alpha3) || Input.GetKey (KeyCode.Alpha4)) {
				
			
			} else {playersText.text = "";}




			if (Input.GetKeyDown (KeyCode.UpArrow)){
				
				if (numPlayers < 4) {
					numPlayers++;
				}			
			}
				
			if (Input.GetKeyDown (KeyCode.DownArrow)) {
				
				if (numPlayers>1){numPlayers--;}
			}

			GetComponentInChildren<Text> ().text = numPlayers.ToString ();
			GameObject.Find("DoNotDestroy").GetComponent<Utilities>().Players = numPlayers;
			print (playersText.text);
		}
	
	}

}