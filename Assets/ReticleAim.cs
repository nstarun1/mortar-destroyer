﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleAim : MonoBehaviour {
	RaycastHit hit;
	public Transform reticle;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Physics.Raycast (transform.position, transform.forward, out hit)) {

			reticle.position = hit.point;

			print ((transform.position - reticle.position).magnitude);



		}

	}
}
