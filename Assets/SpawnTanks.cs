﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

using UnityEngine.UI;

namespace Complete
{


	public class SpawnTanks : MonoBehaviour
	{
		

		[SerializeField] private GameObject _tankPrefab;
		[SerializeField] private List<Color> _tankColors;

		public int _totalPlayers;

		//[HideInInspector]
		public int columns ;
		//[HideInInspector]
		public int rows ;
		public Menu menuScript;
		public GameObject terrain;
		public Color[] tankColorPalette;
		public Texture[] camoTex;


		GameObject tank;
		public List<GameObject> _tanks;
		Rect[] viewPortsMain;
		//public Rect[] viewPortsGun;
		public bool randomSpawn = true;
		Vector2 rightAnchorMin;
		Vector2 rightAnchorMax;
	
		Vector2[] rightAnchorMins;
		Vector2[] rightAnchorMaxes;
		List<Vector3> _nonRandomSpawnLocations;
		public List<Transform> nonRandomSpawnLocations;

		public  void  SetPlayerWindows (List<GameObject>  _tanks)
		{	
		
			float width = 1f / columns;
			float height = 1f / rows;

			int i = 0;
			for (int c = 0; c < columns; c++) {
				for (int r = 0; r < rows; r++) {


					float tankX = c * width;
					float tankY = r * height;

					_tanks [i].GetComponentInChildren<ConfigureMainCamera> ().mainCam.rect = new Rect (tankX, tankY, width, height);

					RectTransform playerCanvas = _tanks [i].GetComponentInChildren<ConfigurePlayerCanvas> ().playerCanvas;
			
					playerCanvas.anchorMin = new Vector2 (tankX, tankY);
					playerCanvas.anchorMax = new Vector2 (tankX + width, tankY + height);

					i++;
					if (i > _tanks.Count - 1) {
						break;
					}

				}
			
			} 

		}






		public void SpawnAll ()



		{
			if (GameObject.Find ("CompleteTank2")) {
				GameObject.Find ("CompleteTank2").SetActive (false);
			}
				
			try {_totalPlayers = GameObject.Find ("DoNotDestroy").GetComponent<Utilities> ().Players;} catch {_totalPlayers = 4;}
			
			Spawn ();

			if (_totalPlayers == 1) {
				columns = 1;
				rows = 1;}
			else  if (_totalPlayers == 2) {
				columns = 1;
				rows = 2;}
			else if(_totalPlayers>2){
				columns = 2;
				rows = 2;
								}
			

			SetPlayerWindows (_tanks);
		}





		public void Spawn ()
		{

			_tanks = new List<GameObject> ();

			if (nonRandomSpawnLocations == null || nonRandomSpawnLocations.Count == 0)
			{
				_nonRandomSpawnLocations = new List<Vector3>();

				_nonRandomSpawnLocations.Add(new Vector3(-94, 2, 12));
				_nonRandomSpawnLocations.Add(new Vector3(104, 2, 145));
				_nonRandomSpawnLocations.Add(new Vector3(-84, 2, 20));
				_nonRandomSpawnLocations.Add(new Vector3(20, 2, -75));
			}
			else
			{
				_nonRandomSpawnLocations = new List<Vector3>();

				foreach (Transform transform in nonRandomSpawnLocations)
				{
					_nonRandomSpawnLocations.Add(transform.position);
				}
			}
			//, new Vector3 (104, 2, 145)]);



			List <string> camoNames = new List<string> ();


	
			Vector3[] locations = terrain.GetComponent<MeshFilter> ().sharedMesh.vertices;
			int verticeCount = locations.Length;	
			Vector3 placementPosition;
//			camoNames.("_CamoBlackTint","_CamoRedTint","_CamoGreenTint","_CamoBlueTint");
			camoNames.Add ("_CamoBlackTint");			
			camoNames.Add ("_CamoRedTint");
			camoNames.Add ("_CamoGreenTint");
			camoNames.Add ("_CamoBlueTint");
			for (int i = 0; i < _totalPlayers; i++) {
				
				if (randomSpawn) {
					
					RaycastHit hit;
					int maxTries = 100;
					int Try = 0;

					while (true) {
						Try++;
						placementPosition = locations [Random.Range (0, verticeCount - 1)] + new Vector3 (0, 10, 0);
						if (Physics.Raycast (placementPosition, Vector3.down, out hit)) {
							break;	}
						if (Try > maxTries) {
							return;
						}

					}

				} else {
			
					placementPosition = _nonRandomSpawnLocations [i];
				}



				tank = Instantiate (_tankPrefab);
			


				


				tank.name = (i + 1).ToString ();
			

				tank.transform.position = placementPosition;



				//Texture texture = camoTex [Random.Range (0, camoTex.Length - 1)];
				foreach (string camoType in camoNames) {

					Vector4 basic = tankColorPalette [Random.Range (0, tankColorPalette.Length - 1)];
					foreach (MeshRenderer s in tank.GetComponentsInChildren<MeshRenderer>()) {
					
						
					
						foreach (var mat in s.materials) {
						
							if (mat.HasProperty ("_CamoBlackTint")) {
								//print (mat.GetVector ("_CamoBlackTint"));
								//mat.SetTexture ("_CamoTex", texture);
								mat.SetVector (camoType, basic + new Vector4 (Random.value - .5f, Random.value - .5f, Random.value - .5f, 0) * .1f);

							}




						}
					}


					//tank.GetComponentInChildren<TankMovement> ().enabled = true;
					//tank.GetComponentInChildren<TankShooting> ().enabled = true;



				}
				_tanks.Add (tank);
			

			}



		}
			
			


		//			tank.GetComponentInChildren<ConfigureMainCamera>().SendMessage ("ConfigureCamera", viewPortsMain [i]);
		//
		//			//Configure right anchors
		//
		//
		//
		//
		//			tank.GetComponentInChildren<ConfigureMiniMapWindow> ().miniMap.anchorMin += rightAnchorMins [i];
		//			tank.GetComponentInChildren<ConfigureMiniMapWindow> ().miniMap.anchorMax += rightAnchorMaxes [i];
		//
		//

















			
			


	}



	//print(menuScript.GetRandomPosition());
}





	




        
//		public void SpawnTank(Vector3 position)
//        {
//            int playerNumber = _tanks.Count + 1;
//			_tankPrefab.SetActive (true);
//
//			GameObject tank = Instantiate (_tankPrefab);
//
//
//			Color c = _tankColors [playerNumber - 1];
//
//			//tank.gameObject.GetComponentInChildren<MiniMapMarker> ()._color = c;

//tank.Configure(_totalPlayers, playerNumber, position, _tankColors[playerNumber - 1]);


		

//_tanks.Add(tank);



//UpdateTanks();



//        }
//
//        public void UpdateTanks()
//        {
//            for (int i = 0; i < _tanks.Count; i++)
//            {
//             //   _tanks[i].Reconfigure();
//            }
//        }
//    }
