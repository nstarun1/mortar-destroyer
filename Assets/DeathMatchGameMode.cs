﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathMatchGameMode : MonoBehaviour
{
    private List<GameObject> _tanks;
    private List<TankHealth> _tankHealths;

    public GameObject winnerCanvas;
    public RawImage winnerImage;

    void Awake()
    {
        winnerCanvas.SetActive(false);
        _tanks = new List<GameObject>();
        _tankHealths = new List<TankHealth>();
    }
    
    public void SetTanks(List<GameObject> tanks)
    {
        _tanks = tanks;

        foreach (GameObject tank in tanks)
        {
            _tankHealths.Add(tank.GetComponent<TankHealth>());
        }

        int totalPlayers = 1;
        
        try
        {
            totalPlayers = GameObject.Find("DoNotDestroy").GetComponent<Utilities>().Players;
        }
        catch
        {
            //
        }

        if (totalPlayers > 1)
        {
            StartCoroutine(MonitorHealth());            
        }
    }

    public IEnumerator MonitorHealth()
    {
        bool noWinner = true;
        List<TankHealth> aliveTanks = new List<TankHealth>();

        while (noWinner)
        {
            aliveTanks.Clear();
            
            foreach(TankHealth tankHealth in _tankHealths)
            {
                if (tankHealth.IsAlive())
                {
                    aliveTanks.Add(tankHealth);
                }
            }

            if (aliveTanks.Count == 1)
            {
                ShowWinner(aliveTanks[0].gameObject);

                noWinner = false;
            }
            
            yield return new WaitForEndOfFrame();
        }
    }

    public void ShowWinner(GameObject tank)
    {
        winnerCanvas.SetActive(true);
        
        Vector2 size = winnerImage.GetComponent<RectTransform>().rect.size;
        Camera winnerCam = tank.transform.Find("TankMainCamera").GetComponent<Camera>();
        winnerCam.aspect = size.x / size.y;
        winnerCam.rect  = new Rect(0, 0, 1, 1);
        RenderTexture contents = new RenderTexture((int) size.x, (int) size.y, 32, RenderTextureFormat.ARGB32);
        tank.transform.Find("TankMainCamera").GetComponent<Camera>().targetTexture = contents;
        winnerImage.texture = contents;
    }
}
