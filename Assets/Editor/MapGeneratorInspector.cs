﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(MapGenerator))]
public class MapGeneratorInspector : Editor {

    // OnInspectorGUI is called on interactions in the editor
    // TODO find out exactly when this is called
    public override void OnInspectorGUI () {
        MapGenerator gen = (MapGenerator)target;
        GenerateOnButton (gen);
        GenerateOnInspectorValueChange (gen);
    }

    private void GenerateOnButton (MapGenerator gen) {
        if (GUILayout.Button ("Generate")) {
            gen.DrawMap ();
        }
    }

    private void GenerateOnInspectorValueChange (MapGenerator gen) {
        if (DrawDefaultInspector ()) {
//            if (gen.autoUpdate) {
//                gen.DrawMap ();
//            }
        }
    }
}
