﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DonDead : MonoBehaviour
{
	public GameObject boom;
	public GameObject skull;
	public Canvas playerCanvas;
	void OnCollisionEnter	(Collision collision)
	{


		GetComponent<Rigidbody> ().AddExplosionForce (10, transform.position, 5, 1);

		if (collision.collider.tag == "Shell") {
	

			GameObject b = Instantiate (boom);
			//Destroy (boom, 3f);
				
			b.transform.position = transform.position;


			skull.SetActive (true);



			foreach (Transform t in  transform.root.gameObject.GetComponentInChildren<Transform>()) {
				if (t.name != "GUICanvas") {
				
					t.gameObject.SetActive (false);
				} 
			}


//			foreach (Transform t in playerCanvas.GetComponentsInChildren<RectTransform>()) {
//					if (t.name != "Skull") 
//				{
//						t.gameObject.SetActive (false);}
//				}


			}



			//DestroyObject (this.transform.root.gameObject, .2f);
		}
	

	}


	// Update is called once per frame




